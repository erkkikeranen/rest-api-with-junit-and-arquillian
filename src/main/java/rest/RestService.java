/* RestService.java */

package rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import data.DataRepository;
import model.Entity;
/**
 * RestService.java
 * <p>
 * Simple JAX-RS Rest Service providing CRUD operations to an in-memory data repository.
 * <p>
 * Example written as part of bachelor of engineering thesis.
 * @author Erkki Keränen
*/
@Path("/api")
@RequestScoped
public class RestService {

	/**
	 * The DataRepository that is injected for every service request.
	 */
	@Inject 
	DataRepository dataRepository;
	
	/**
	 * Dependency injector for mock-objects and Arquillian tests
	 * @param dataRepository For testing instantiated DataRepository object or mock.
	 */
	public void setDataRepository(DataRepository dataRepository) {
		this.dataRepository = dataRepository;	
	}	
	
	/**
	 * HTTP POST method providing mapping to DataRepository CRUD Create.
	 * <p>
	 * For the simplicity of this example, this method always succeeds.
	 * @param entity The entity to add to repository.
	 * @return HTTP Response with HTTP Status 201 Created.
	 */
	@POST
	@Consumes({MediaType.APPLICATION_JSON, 
				MediaType.APPLICATION_XML})
	public Response create(Entity entity)
	{
		dataRepository.create(entity);
		return Response.created(null).build();
	}
	
	/**
	 * HTTP GET method providing mapping to DataRepository CRUD Read (All)
	 * @return HTPP Response with HTTP Status 200 Ok with body containing all entities from the DataRepository.
	 */
	@GET 
	@Produces({MediaType.APPLICATION_JSON,
				MediaType.APPLICATION_XML})
	public Response readAll()
	{
		return Response.
				ok().
				entity(dataRepository.readAll()).
				build();
	}
	
	/**
	 * HTTP GET method providing mapping to DataRepository CRUD Read
	 * @param id The id of the entity to read. 
	 * @return On success, HTPP Response with HTTP Status 200 Ok with body containing the entity. 
	 * If fail, HTTP Response with HTTP Status 404 Not Found.
	 */
	@GET
	@Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON, 
				MediaType.APPLICATION_XML})
	public Response read(@PathParam("id") int id)
	{
		Entity entity = dataRepository.read(id);
		if (entity == null)
			return Response
					.status(404).build();
		
		return Response
				.ok().
				entity(entity).
				build();
	}
	/**
	 * HTTP PUT method providing mapping to DataRepository CRUD Update
	 * @param entity The new contents containing the actual id, which defines the entity for the operation.
	 * @return On success, HTTP Status 200 Ok. If fail, HTTP Status 400 Bad Request.
	 */
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, 
				MediaType.APPLICATION_XML})
	public Response update(Entity entity)
	{
		if (dataRepository.update(entity)) 
			return Response.ok().build();
		
		return Response.status(400).build();
	}
	
	/**
	 * HTTP DELETE method providing mapping to DataRepository CRUD Delete 
	 * @param id The id of the entity to delete.
	 * @return On success, HTTP Status 204 No Content. If fail, HTTP Status 404 Not Found.
	 */
	@DELETE
	@Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON, 
				MediaType.APPLICATION_XML})
	public Response delete(@PathParam("id") int id)
	{
		if (dataRepository.delete(id))
			return Response.noContent().build();
		
		return Response.status(404).build();
	}
}