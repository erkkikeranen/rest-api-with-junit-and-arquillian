package data;

import javax.enterprise.context.ApplicationScoped;
import model.Entity;
import model.Entities;

/**
 * DataRepository.java
 * <p>
 * A simple in-memory repository for storing and manipulating data with CRUD operations.
 * <p>
 * Example written as part of bachelor of engineering thesis.
 * @author Erkki Keränen
*/
@ApplicationScoped
public class DataRepository {

	/**
	 * The next available id for the data, that is used for 
	 * populating the id field of an entity.
	 */
	private int nextId;
	
	/**
	 * The object containing the data used by the DataRepository.
	 */
	private Entities data;
	
	public DataRepository()
	{
		data = new Entities();
		nextId = 0;
	}
	
	/**
	 * DataRepository CRUD Create
	 * @param entity Entity to add in the DataRepository.
	 */
	public void create(Entity entity)
	{
		entity.setId(nextId);
		data.entities.add(entity);
		nextId++;
	}
	
	/**
	 * DataRepository CRUD Read (All)
	 * @return All entities.
	 */
	public Entities readAll()
	{
		return data;
	}
	
	/**
	 * DataRepository CRUD Read
	 * @param id The entity with selected id to retrieve.
	 * @return The entity with given id, or null if not found. 
	 */
	public Entity read(int id)
	{
		Entity result = null;
		
		for (Entity entity : data.entities)
		{
			if (entity.getId() == id) 
			{
				result = entity;
				break;
			}
		}
		return result;
	}
	
	/**
	 * DataRepository CRUD Update
	 * @param entity Entity containing valid id with new values.
	 * @return True if success, false if fail.
	 */
	public boolean update(Entity entity)
	{
		for (int i=0;i<data.entities.size();i++)
		{
			if (data.entities.get(i).getId() == entity.getId())
			{
				data.entities.get(i).setName(entity.getName());
				return true;
			}
		}
		return false;
	}
	
	/**
	 * DataRepository CRUD Delete
	 * @param id The id of the given entity to delete.
	 * @return True if success, false if fail.
	 */
	public boolean delete(int id)
	{
		for (int i=0;i<data.entities.size();i++)
		{
			if (data.entities.get(i).getId() == id)
			{
				data.entities.remove(i);
				return true;
			}
		}
		return false;
	}
}