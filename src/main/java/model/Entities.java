package model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entities.java 
 * <p>
 * Class for containing a collection of objects of class Entity.
 * <p>
 * Example written as part of bachelor of engineering thesis.
 * @author Erkki Keränen 
*/
@XmlRootElement (name = "Entities")
public class Entities {

	/**
	 * The list that contains the elements as objects of type Entity.
	 */
	@XmlElement(name = "Entity")
	public List<Entity> entities;
	
	public Entities()
	{
		entities = new ArrayList<Entity>();
	}
}