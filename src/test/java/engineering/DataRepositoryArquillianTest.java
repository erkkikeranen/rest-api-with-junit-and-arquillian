package engineering;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import data.DataRepository;
import model.Entity;

/**
 * DataRepositoryArquillianTest.java
 * <p>
 * Demonstrates unit testing of the in-memory Data Repository.
 * Same as DataReposiroryJUnitTest.java, but runs with Arquillian test runner.
 * @author Erkki Keränen
 *
 */
@RunWith(Arquillian.class)
public class DataRepositoryArquillianTest {
	
	
	DataRepository testDataRepository;
	Entity testEntity1;
	Entity testEntity2;
	
	final static int TEST_ENTITY1_ID = 123;
	final static String TEST_ENTITY1_NAME = "Test Name1";
	
	
	final static int TEST_ENTITY2_ID = 456;
	final static String TEST_ENTITY2_NAME = "Test Name2";
	
	final static String TEST_ENTITY3_NAME = "Modified Name";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {	
		/* Time and resource expensive setup 
		 * before all test runs of
		 * this class should be put here
		 */
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		/* Time and resource expensive tear 
		 * down after all test runs of
		 * this class should be put here
		 */
	}

	@Before
	public void setUp() throws Exception {
		/* We reconstruct the datarepository and test entities before all runs */
		testDataRepository = new DataRepository();
		
		testEntity1 = new Entity();
		testEntity1.setId(TEST_ENTITY1_ID);
		testEntity1.setName(TEST_ENTITY1_NAME);
		
		testEntity2 = new Entity();
		testEntity2.setId(TEST_ENTITY2_ID);
		testEntity2.setName(TEST_ENTITY2_NAME);
	}

	@After
	public void tearDown() throws Exception {
		/* We empty the datarepository and test entities after all runs */
		testDataRepository = null;
		testEntity1 = null;
		testEntity2 = null;
	}

	@Test
	public void testDataRepositoryConstructorConstructsEmptyContainerNotNull() 
	{
		assertNotNull("DataRepository was not constructed correctly", testDataRepository.readAll());
	}
	
	@Test
	public void testDataRepositoryEmptyAfterConstruction()
	{
		assertEquals("The repository was not empty after construction", 
				0,
				testDataRepository.readAll().entities.size());
	}

	@Test
	public void testDataRepositoryCreateAndVerifyAmountOfEntities() {
		
		assertEquals("The repository was not empty after construction", 
				0,
				testDataRepository.readAll().entities.size());
		
		testDataRepository.create(testEntity1);
		assertEquals("Wrong amount of entities in repository", 
				1,
				testDataRepository.readAll().entities.size());
		
		testDataRepository.create(testEntity2);
		assertEquals("Wrong amount of entities in repository", 
				2,
				testDataRepository.readAll().entities.size());
		
	}

	@Test
	public void testCheckCreatedDataWasCorrect() {
		
		testDataRepository.create(testEntity1);
		testDataRepository.create(testEntity2);
		
		assertEquals("There was wrong amount of entities in the repository", 
				2,
				testDataRepository.readAll().entities.size());
		
		assertEquals("The entity contained incorrect data", 
				TEST_ENTITY1_NAME,
				testDataRepository.readAll().entities.get(0).getName());
		
		assertEquals("The entity contained incorrect data", 
				TEST_ENTITY2_NAME,
				testDataRepository.readAll().entities.get(1).getName());
		
	}

	@Test
	public void testMostRecentCreatedEntitiesCorrect() {
		
		testDataRepository.create(testEntity1);
		
		assertEquals("Most recent entity did not equal the last created",
				TEST_ENTITY1_NAME,
				testDataRepository.read(testDataRepository.readAll().entities.size()-1).getName());
	
		testDataRepository.create(testEntity2);
		
		assertEquals("Most recent entity did not equal the last created",
				TEST_ENTITY2_NAME,
				testDataRepository.read(testDataRepository.readAll().entities.size()-1).getName());
	}
	
	@Test
	public void testGetNullWhenReadingFirstEntityFromEmptyRepository() {
		
		assertNull("The requested entity that was not found did not result as null",
				testDataRepository.read(0));
	}
	
	@Test(expected=NullPointerException.class)
	public void testThrowNullPointerExceptionWhenUsingGetNameOnNullEntityInEmptyRepository() {
		@SuppressWarnings("unused")
		String testTheNameReadFromEmptyEntityInEmptyRepository = testDataRepository.read(0).getName();	
	}

	@Test
	public void testUpdateEntity1() {
		
		testDataRepository.create(testEntity1);
		testDataRepository.create(testEntity2);
		
		assertEquals("The requested entity was not the same as expected",
				TEST_ENTITY1_NAME,
				testDataRepository.read(0).getName());
		assertEquals("The requested entity was not the same as expected",
				TEST_ENTITY2_NAME,
				testDataRepository.read(1).getName());
		
		Entity testEntityForUpdatingData = new Entity();
		
		testEntityForUpdatingData.setId(0);
		testEntityForUpdatingData.setName(TEST_ENTITY3_NAME);
		
		assertTrue("Update should succeed", 
				testDataRepository.update(testEntityForUpdatingData));
		assertEquals("The requested entity was not changed", 
				TEST_ENTITY3_NAME, 
				testDataRepository.read(0).getName());
		assertEquals("Other entity than requested was changed",
				TEST_ENTITY2_NAME,
				testDataRepository.read(1).getName());
	}
	
	@Test
	public void testUpdateEntity2() {
		
		testDataRepository.create(testEntity1);
		testDataRepository.create(testEntity2);
		
		assertEquals("The requested entity was not the same as expected",
				TEST_ENTITY1_NAME,
				testDataRepository.read(0).getName());
		assertEquals("The requested entity was not the same as expected",
				TEST_ENTITY2_NAME,
				testDataRepository.read(1).getName());
		
		Entity testEntityForUpdatingData = new Entity();
		
		testEntityForUpdatingData.setId(1);
		testEntityForUpdatingData.setName(TEST_ENTITY3_NAME);
		
		assertTrue("Update should succeed", 
				testDataRepository.update(testEntityForUpdatingData));
		assertEquals("The requested entity was not changed", 
				TEST_ENTITY3_NAME, 
				testDataRepository.read(1).getName());
		assertEquals("Other entity than requested was changed",
				TEST_ENTITY1_NAME,
				testDataRepository.read(0).getName());
	}
	
	@Test
	public void testUpdateEntityThatDoesNotExist1() {
		Entity testEntityForUpdatingData = new Entity();
		
		testEntityForUpdatingData.setId(0);
		testEntityForUpdatingData.setName(TEST_ENTITY3_NAME);
		
		assertEquals("There was wrong amount of entities in the repository",
				0,
				testDataRepository.readAll().entities.size());
		
		assertFalse("Update should not have succeeded on non-existing entity",
				testDataRepository.update(testEntityForUpdatingData));
		
		
	}
	
	@Test
	public void testUpdateEntityThatDoesNotExist2() {
		
		testDataRepository.create(testEntity1);
		Entity testEntityForUpdatingData = new Entity();
		
		testEntityForUpdatingData.setId(1);
		testEntityForUpdatingData.setName(TEST_ENTITY3_NAME);
		
		assertEquals("There was wrong amount of entities in the repository",
				1,
				testDataRepository.readAll().entities.size());
		
		assertFalse("Update should not have succeeded on non-existing entity",
				testDataRepository.update(testEntityForUpdatingData));
		
		
	}
	
	@Test
	public void testUpdateEntityThatDoesNotExist3() {
		
		testDataRepository.create(testEntity1);
		testDataRepository.delete(0);
		testDataRepository.create(testEntity2);
		Entity testEntityForUpdatingData = new Entity();
		
		testEntityForUpdatingData.setId(0);
		testEntityForUpdatingData.setName(TEST_ENTITY3_NAME);
		
		assertEquals("There was wrong amount of entities in the repository",
				1,
				testDataRepository.readAll().entities.size());
		
		assertFalse("Update should not have succeeded on non-existing entity",
				testDataRepository.update(testEntityForUpdatingData));

	}

	@Test
	public void testDeleteObjectThatDoesNotExist() {
		
		assertFalse("There may not exist an object with the requested id",
				testDataRepository.delete(0));
	}
	
	@Test
	public void testDeleteEntity() {
		
		testDataRepository.create(testEntity1);
		
		assertTrue("Entity could not be removed from repository with requested id",
				testDataRepository.delete(0));
	}
	
	@Test
	public void testDeleteEntititiesFirstInFirstOutOrder() {
		
		testDataRepository.create(testEntity1);
		testDataRepository.create(testEntity2);
		
		assertTrue("Entity could not be removed from repository with requested id",
				testDataRepository.delete(0));
		
		assertTrue("Entity could not be removed from repository with requested id",
				testDataRepository.delete(1));
	}
	
	@Test
	public void testDeleteEntitiesLastInFirstOutOrder() {
		
		testDataRepository.create(testEntity1);
		testDataRepository.create(testEntity2);
		
		assertTrue("Entity could not be removed from repository with requested id",
				testDataRepository.delete(1));
		
		assertTrue("Entity could not be removed from repository with requested id",
				testDataRepository.delete(0));
	}
	
	@Test
	public void testCheckIdNumberingAfterCreatingSequentialEntities() {
	
		for (int i=0;i<5;i++)
		{
			testDataRepository.create(testEntity1);
			assertEquals("Id's were not created in sequential order", 
					i, 
					testDataRepository.read(i).getId());
		}
	}
	
	@Test
	public void testCheckIdNumberingAfterCreatingSequnceAndDeletingSequence() {
	
		for (int i=0;i<5;i++)
		{
			testDataRepository.create(testEntity1);
			testDataRepository.delete(i);
		}
		
		testDataRepository.create(testEntity1);
		
		assertEquals("Wrong amount of entities in repository", 
				1, 
				testDataRepository.readAll().entities.size());
		assertEquals("Next assigned id was not correct",
				5,
				testDataRepository.readAll().entities.get(0).getId());
	}
	
	/*
	 * This test fails because the problem 
	 * is not addressed in DataRepository 
	 * implementation.
	 * This demonstrates the problems
	 * mock objects cannot address,
	 * but can be caught in integration
	 * test.
	 * Hence test ignored.
	 */
	@Ignore
	@Test
	public void testCreateNullObject()
	{
		testDataRepository.create(null);
		// Assertions missing
	}

}
