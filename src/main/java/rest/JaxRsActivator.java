package rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * JaxRsActivator.java 
 * <p>
 * RESTEasy "no XML" approach for activating JAX-RS.
 * <p>
 * Example written as part of bachelor of engineering thesis.
 * @author Erkki Keränen
 *  
*/
@ApplicationPath("/rest")
public class JaxRsActivator extends Application{
	/* Intentionally left blank */ 
}
