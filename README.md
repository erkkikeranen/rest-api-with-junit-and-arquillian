### Simple Jax-RS REST-Api with unit tests and integration tests

https://gitlab.com/erkkikeranen/rest-api-with-junit-and-arquillian

This repository contains the example code for my bachelor of engineering in software engineering degree project. This projects aim is not to be fully correct
in all aspects. Instead it tries to demonstrate the difference on writing unit tests with mocks and integration tests, and what kind of results these two
types/phases of testing can deliver.


* Simple CRUD mapping to in memory repository
* JUnit unit tests with Mockito and Arquillian integration tests for comparision


Remember to set $JBOSS_HOME:
```
export JBOSS_HOME=/opt/wildfly-10.1.0-Final/
```

(or whatever is your path to wildfly, if local instance)

To run unit tests (Junit & Mockito) on local/remote wildfly instance:

```
mvn test -Punit-test
```

To run all tests (Junit & Mockito and Arquillian) on local/remote wildfly instance:

```
mvn integration-test -Parquillian-wildfly-remote
```

To deploy on wildfly
```
mvn wildfly:deploy
```
The deployment should appear on http://localhost:8080/restapiexample/rest/api (or whatever your configuration looks alike)

---
## Sources of information

* Using Forge, existing Java EE Project was configured for Arquillian with help from 
http://blog.arungupta.me/enable-arquillian-existing-javaee-project-techtip54/
* Separating tests in Maven with help from https://thomassundberg.wordpress.com/2012/08/21/separating-tests-in-maven/