package model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/** Entity.java 
 * <p>
 * Class for containing an entity.
 * <p>
 * Example written as part of bachelor of engineering thesis.
 * @author Erkki Keränen 
*/
@XmlRootElement(name = "Entity")
public class Entity {

	/**
	 * The id of the entity.
	 */
	private int id;
	
	/**
	 * The name (a field) of the entity
	 */
	private String name;
	
	@XmlElement
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@XmlElement
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}