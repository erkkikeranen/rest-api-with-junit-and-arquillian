package engineering;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import data.DataRepository;
import model.Entity;
import model.Entities;
import rest.JaxRsActivator;
import rest.RestService;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;

import static org.junit.Assert.*;

/**
 * RestSserviceArquillianTest.java
 * <p>
 * Demonstrates testing of RestService with Arquillian, deploying a archive for test including all needed dependencies.
 * @author Erkki Keränen
 *
 */
@RunWith(Arquillian.class)
public class RestServiceArquillianTest {

	final static String TEST_ENTITY1_NAME = "Test Name1";
	final static int TEST_ENTITY1_ID = 0;
	
	final static String TEST_ENTITY2_NAME = "Test Name2";
	final static int TEST_ENTITY2_ID = 1;
	
	Entity testEntity1;
	Entity testEntity2;

	/*
	 * For running these tests, we must empty the
	 * Data Repository between tests. Otherwise
	 * Dependency injection would give the same
	 * @ApplicationScoped repository for all test
	 * runs.
	 */
	DataRepository testDataRepository;
	
	@Inject
	RestService testRestService;
	
	@Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
            .addClasses(JaxRsActivator.class, 
            		RestService.class, 
            		DataRepository.class, 
            		Entity.class, 
            		Entities.class);
    }
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	/*
	 * For Arquillian tests, we create the test
	 * data in every run with the create method
	 * of Rest Service.
	 * We can test the effects on the whole 
	 * integration from request to HTTP-status.
	 * 
	 * A new, empty datarepository is injected 
	 * for every test.
	 */

	@Before
	public void setUp() throws Exception {
		
		testDataRepository = new DataRepository();
		testRestService.setDataRepository(testDataRepository);
		
		testEntity1 = new Entity();
		testEntity1.setName(TEST_ENTITY1_NAME);
		testEntity1.setId(TEST_ENTITY1_ID);
		
		testEntity2 = new Entity();
		testEntity2.setName(TEST_ENTITY2_NAME);
		testEntity2.setId(TEST_ENTITY2_ID);
	}

	@After
	public void tearDown() throws Exception {
		
		testDataRepository = null;
		testEntity1 = null;
		testEntity2 = null;
	}

	@Test
	public void testRestServiceReadAllStatus200Ok() {
	
		Response testResponse;
		
		testResponse = testRestService.readAll();
		
		assertEquals("Status was not 200 Ok",
				200,
				testResponse.getStatus());
	}
	
	
	/*
	 * This test verifies that the repository
	 * is empty in beginning of test run.
	 */
	@Test
	public void testRestServiceReadAllFromEmptyRepositoryReturnsZeroAnd200Ok()
	{
		Response testResponse;
		
		testResponse = testRestService.readAll();
		
		Entities testResponseEntities = (Entities) testResponse.getEntity();
		
		assertEquals("Wrong amount of entities in response",
				0,
				testResponseEntities.entities.size());
		assertEquals("Status was not 200 OK",
				200,
				testResponse.getStatus());
	}
	
	@Test
	public void testRestServiceCreateStatus201Created()
	{
		Response testResponse;
		
		testResponse = testRestService.create(testEntity1);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
	}

	@Test
	public void testRestServiceCreateOneThenReadAllReturnEntitiesWithOneEntity()
	{
		Response testResponse;
		
		testResponse = testRestService.create(testEntity1);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
		
		testResponse = null;
		testResponse = testRestService.readAll();
		
		Entities testResponseEntities = (Entities) testResponse.getEntity();
		
		assertEquals("Wrong amount of entities in response",
				1,
				testResponseEntities.entities.size());
		assertEquals("Status was not 200 OK",
				200,
				testResponse.getStatus());
	}
	
	@Test
	public void testRestServiceCreateOneReaditWithReadAllMethod()
	{
		Response testResponse;
		
		testResponse = testRestService.create(testEntity1);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
		
		testResponse = null;
		testResponse = testRestService.readAll();
		
		Entities testResponseEntities = (Entities) testResponse.getEntity();
		
		assertEquals("Wrong amount of entities in response",
				1,
				testResponseEntities.entities.size());
		assertEquals("Wrong id",
				TEST_ENTITY1_ID,
				testResponseEntities.entities.get(0).getId());
		assertEquals("Wrong name",
				TEST_ENTITY1_NAME,
				testResponseEntities.entities.get(0).getName());
		assertEquals("Status was not 200 OK",
				200,
				testResponse.getStatus());
	}
	
	@Test
	public void testRestServiceCreateOneThenReadIt()
	{
		Response testResponse;
		
		testResponse = testRestService.create(testEntity1);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
		
		testResponse = null;
		testResponse = testRestService.read(0);
		
		Entity testResponseEntity = (Entity) testResponse.getEntity();

		assertEquals("Wrong id",
				TEST_ENTITY1_ID,
				testResponseEntity.getId());
		assertEquals("Wrong name",
				TEST_ENTITY1_NAME,
				testResponseEntity.getName());
		assertEquals("Status was not 200 OK",
				200,
				testResponse.getStatus());
	}
	
	@Test
	public void testRestServiceReadNonExisting404NotFound()
	{
		Response testResponse;
		
		testResponse = testRestService.read(1);
		assertEquals("Status was not 404 Not Found",
				404,
				testResponse.getStatus());
	}
	
	/*
	 * Compared to Mock-based testing,
	 * in this integration test following
	 * will fail in NullPointerException.
	 * 
	 * So maybe we missed testing this 
	 * issue in the Datarepository tests?
	 * 
	 * (The assertion is also wrong)
	 * 
	 */
	@Ignore
	@Test
	public void testRestServiceCreateEntityNullObject()
	{
		Response testResponse;
		
		testResponse = testRestService.create(null);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
	}
	
	@Test
	public void testRestServiceCreateEntityThenDeleteEntityStatus204NoContent()
	{
		Response testResponse;
		
		testResponse = testRestService.create(testEntity1);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
		
		testResponse = null;
		testResponse = testRestService.readAll();
		Entities testResponseEntities = (Entities) testResponse.getEntity();
		
		assertEquals("Wrong number of entities in response",
				1,
				testResponseEntities.entities.size());
		
		testResponse = null;
		testResponse = testRestService.delete(0);
		assertEquals("Status was not 204 No Content",
				204,
				testResponse.getStatus());
		
	}
	
	@Test
	public void testRestServiceCreateEntityThenDeleteNonExistingStatus404NotFound1()
	{
		Response testResponse;
		
		testResponse = testRestService.create(testEntity1);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
		
		testResponse = null;
		testResponse = testRestService.readAll();
		Entities testResponseEntities = (Entities) testResponse.getEntity();
		assertEquals("Wrong number of entities in response",
				1,
				testResponseEntities.entities.size());
		
		testResponse = null;
		testResponse = testRestService.delete(1);
		assertEquals("Status was not 404 Not Found",
				404,
				testResponse.getStatus());
		
	}
	
	@Test
	public void testRestServiceCreateEntityThenDeleteNonExistingStatus404NotFound2()
	{
		Response testResponse;
		
		testResponse = testRestService.create(testEntity1);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
		
		testResponse = null;
		testResponse = testRestService.readAll();
		Entities testResponseEntities = (Entities) testResponse.getEntity();
		assertEquals("Wrong number of entities in response",
				1,
				testResponseEntities.entities.size());
		
		testResponse = null;
		testResponse = testRestService.delete(-1);
		assertEquals("Status was not 404 Not Found",
				404,
				testResponse.getStatus());
		
	}
	
	@Test
	public void testRestServiceCreateOneThenUpdateItStatus200()
	{
		Response testResponse;
		
		testResponse = testRestService.create(testEntity1);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
		
		testResponse = null;
		testResponse = testRestService.readAll();
		Entities testResponseEntities = (Entities) testResponse.getEntity();
		assertEquals("Wrong number of entities in response",
				1,
				testResponseEntities.entities.size());
				
		Entity testEntityForUpdateOperation = new Entity();
		
		testEntityForUpdateOperation.setId(0);
		testEntityForUpdateOperation.setName("Updated Name");

		testResponse = null;
		testResponse = testRestService.update(testEntityForUpdateOperation);
		assertEquals("Status was not 200 Ok",
				200,
				testResponse.getStatus());
		
		testResponse = null;
		testResponse = testRestService.read(0);
		Entity testResponseEntity = (Entity) testResponse.getEntity();
		assertEquals("Wrong id",
				0,
				testResponseEntity.getId());
		assertEquals("Wrong name",
				testEntityForUpdateOperation.getName(),
				testResponseEntity.getName());
		
		
	}
	
	@Test
	public void testRestServiceCreateOneUpdateThenNonExistingStatus400BadRequest()
	{
		Response testResponse;
		
		testResponse = testRestService.create(testEntity1);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
		
		testResponse = null;
		testResponse = testRestService.readAll();
		Entities testResponseEntities = (Entities) testResponse.getEntity();
		assertEquals("Wrong number of entities in response",
				1,
				testResponseEntities.entities.size());
				
		Entity testEntityForUpdateOperation = new Entity();
		
		testEntityForUpdateOperation.setId(3);
		testEntityForUpdateOperation.setName("Updated Name");

		testResponse = null;
		testResponse = testRestService.update(testEntityForUpdateOperation);
		assertEquals("Status was not 400 Bad Request",
				400,
				testResponse.getStatus());
	}
}
