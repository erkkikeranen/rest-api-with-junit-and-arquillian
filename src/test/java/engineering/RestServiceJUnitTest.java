package engineering;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import data.DataRepository;
import model.Entities;
import model.Entity;
import rest.RestService;

/**
 * RestServiceJUnitTest.java
 * <p>
 * Demonstrates unit testing of RestService using mock objects to mock the behavior of dependencies. 
 * @author Erkki Keränen
 *
 */
public class RestServiceJUnitTest {
	
	RestService testRestService;
	DataRepository mockDataRepository;
	
	Entities testEntitiesWithOneEntity;
	
	Entity testEntity1,
			testEntity2;
	
	final static String TEST_ENTITY1_NAME = "Test Nimi1";
	final static int TEST_ENTITY1_ID = 0;
	
	final static String TEST_ENTITY2_NAME = "Test Nimi2";
	final static int TEST_ENTITY2_ID = 1;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/*
	 * With mocking, the assumed data 
	 * must be created, so the functionality
	 * can be inspected
	 */
	
	@Before
	public void setUp() throws Exception {
		
		testEntitiesWithOneEntity = new Entities();
		testEntity1 = new Entity();
		testEntity1.setName(TEST_ENTITY1_NAME);
		testEntity1.setId(TEST_ENTITY1_ID);
		testEntitiesWithOneEntity.entities.add(testEntity1);
		
		testEntity2 = new Entity();
		testEntity2.setName(TEST_ENTITY2_NAME);
		testEntity2.setId(TEST_ENTITY2_ID);
		
		mockDataRepository = mock(DataRepository.class);
		when(mockDataRepository.readAll()).
			thenReturn(testEntitiesWithOneEntity);
		
		when(mockDataRepository.read(0)).
			thenReturn(testEntity1);
		
		when(mockDataRepository.read(1)).
			thenReturn(null);
		
		when(mockDataRepository.delete(0)).
			thenReturn(true);
		
		when(mockDataRepository.delete(1)).
			thenReturn(false);
		
		when(mockDataRepository.update(testEntity1)).
		thenReturn(true);
	
		when(mockDataRepository.update(testEntity2)).
			thenReturn(false);
		
		testRestService = new RestService();
		
		testRestService.setDataRepository(mockDataRepository);
		
	}

	@After
	public void tearDown() throws Exception {
		testRestService = null;
		mockDataRepository = null;
		
		testEntitiesWithOneEntity = null;
		testEntity1 = null;
		testEntity2 = null;
	}
	
	@Test
	public void testRestServiceCreateEntityStatus201Created() throws Exception{
		
		Response testResponse = testRestService.create(testEntity2);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
		verify(mockDataRepository).create(testEntity2);
	}
	
	/* 
	 * The following test cannot
	 * be tested reliably with 
	 * mocks. Compare to Arquillian
	 * integration test that throws
	 * NullPointerException
	 * 
	 * The outcome of this kind of 
	 * tests with mocking is 
	 * questionable.
	 * 
	 * The assertion is also 
	 * based on false assumptions
	 * and should be corrected.
	 * 
	 */
	
	@Test
	public void testRestServiceCreateNullObject() throws Exception{

		Response testResponse = testRestService.create(null);
		assertEquals("Status was not 201 Created",
				201,
				testResponse.getStatus());
		verify(mockDataRepository).create(null);
	}
	
	@Test
	public void testRestServiceReadAllReturnsTestEntityInEntitiesStatus200Ok() throws Exception{
		
		Response testResponse = testRestService.readAll();
		Entities testResponseEntities = (Entities) testResponse.getEntity();
		assertEquals("Wrong number of entities was returned",
				1,
				testResponseEntities.entities.size());
		assertEquals("Entity had wrong id",
				TEST_ENTITY1_ID,
				testResponseEntities.entities.get(0).getId());
		assertEquals("Entity had wrong name",
				TEST_ENTITY1_NAME,
				testResponseEntities.entities.get(0).getName());
		assertEquals("Status was not 200 OK",
				200,
				testResponse.getStatus());
		verify(mockDataRepository).readAll();
	}
	
	@Test
	public void testRestServiceReadReturnsTestEntityStatus200Ok() throws Exception{
		Response testResponse = testRestService.read(0);
		
		Entity testResponseEntity = (Entity) testResponse.getEntity();
		assertEquals("Entity had wrong id",
				TEST_ENTITY1_ID,
				testResponseEntity.getId());
		assertEquals("Entity had wrong name",
				TEST_ENTITY1_NAME,
				testResponseEntity.getName());
		assertEquals("Status was not 200 Ok",
				200,
				testResponse.getStatus());
		verify(mockDataRepository).read(0);
	}
	
	@Test
	public void testRestServiceReadNonExistingStatus404NotFound() throws Exception{
		Response testResponse = testRestService.read(1);
		
		Entity testResponseEntity = (Entity) testResponse.getEntity();
		assertEquals("Body should not have content",
				null,
				testResponseEntity);
		assertEquals("Status was not 404 Not Found",
				404,
				testResponse.getStatus());
		verify(mockDataRepository).read(1);
	}
	
	@Test
	public void testRestServiceUpdateExistingStatus200Ok() throws Exception{
		Response testResponse = testRestService.update(testEntity1);
		assertEquals("Status was not 200 Ok",
				200,
				testResponse.getStatus());
		verify(mockDataRepository).update(testEntity1);
	}
	
	@Test
	public void testRestServiceUpdateNonExistingStatus400BadRequest() throws Exception{
		Response testResponse = testRestService.update(testEntity2);
		assertEquals("Status was not 400 Bad Request",
				400,
				testResponse.getStatus());
		verify(mockDataRepository).update(testEntity2);
	}
	
	@Test
	public void testRestServiceDeleteExistingStatus204NoContent() throws Exception{
		Response testResponse = testRestService.delete(0);
		assertEquals("Status was not 204 No Content",
				204,
				testResponse.getStatus());
		verify(mockDataRepository).delete(0);
	}
	
	@Test
	public void testRestServiceDeleteNonExistingStatus404NotFound() throws Exception{
		Response testResponse = testRestService.delete(1);
		assertEquals("Status was not 404 Not Found",
				404,
				testResponse.getStatus());
		verify(mockDataRepository).delete(1);
	}
	
	
}
